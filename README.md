# Ansible Network Automation Workshop 201
**This is documentation for Ansible Automation Platform 2**

The Ansible Network Automation workshop 201 is a two day comprehensive intermediate guide to automating popular network data center devices from Arista, Cisco and Juniper via Ansible AAP. On day one, you'll deepdive into ansible network automation and levelset on key ansible techniques and best practices using ansible navigator and playbooks. On day two, you’ll explore many popular network automation use cases and build on day one by applying network automation using the Ansible automation controller. Day two culminates with a fun and challenging capstone project to bring it all together from two days of emersive training.

## Presentation

Want the Presentation Deck?  Its right here:
- [Ansible Network Automation Workshop 201 Day 1 Deck](https://ansible.github.io/workshops/decks/ansible_network.pdf) PDF

- [Ansible Network Automation Workshop 201 Day 2 Deck](https://gitlab.com/redhatautomation/ansible-network-automation-workshop-201/-/blob/main/day2/lab_guide/Network_Automation_Workshop_Deck_201_Day2.pdf) PDF

## Ansible Network Automation Exercises
* [Day 1 ](./day1/README.md)
* [Day 2 ](./day2/README.md)

