### The setup.yml playbook will configure the all of the necessary controller constructs up to 6-automated-netops-validation

Run the `ansible-navigator` command with the `run` argument and -m stdout as well as `extra-vars` -e

- **Note the cloud_pass variable**

```bash
$ ansible-navigator run setup.yml -m stdout -e "username=redacted token=redacted password=redacted cloud_pass=redacted" 